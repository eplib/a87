<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A87487">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation of pardon</title>
    <title>Proclamations. 1688-11-22</title>
    <author>England and Wales. Sovereign (1685-1688 : James II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A87487 of text R222837 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing J362). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A87487</idno>
    <idno type="STC">Wing J362</idno>
    <idno type="STC">ESTC R222837</idno>
    <idno type="EEBO-CITATION">99896515</idno>
    <idno type="PROQUEST">99896515</idno>
    <idno type="VID">133549</idno>
    <idno type="PROQUESTGOID">2240941292</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A87487)</note>
    <note>Transcribed from: (Early English Books Online ; image set 133549)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2430:5)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation of pardon</title>
      <title>Proclamations. 1688-11-22</title>
      <author>England and Wales. Sovereign (1685-1688 : James II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>printed by Charles Bill, Henry Hills, and Thomas Newcomb, printers to the Kings most Excellent Majesty,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1688.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Salisbury the 22th day of November 1688. in the fourth year of our reign".</note>
      <note>Offering a pardon to those who have joined the Prince of Orange.</note>
      <note>Steele notation: Arms 108; up Orange li-.</note>
      <note>Reproduction of original in the Folger Shakespeare Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Pardon -- England -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1660-1688 -- Early works to 1800.</term>
     <term>Great Britain -- History -- James II, 1685-1688 -- Early works to 1800.</term>
     <term>Broadsides -- England</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King, a proclamation of pardon.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>387</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-09</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-10</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A87487-t">
  <body xml:id="A87487-e0">
   <div type="proclamation" xml:id="A87487-e10">
    <pb facs="tcp:133549:1" xml:id="A87487-001-a"/>
    <head xml:id="A87487-e20">
     <w lemma="by" pos="acp" xml:id="A87487-001-a-0010">By</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-0020">the</w>
     <w lemma="king" pos="n1" xml:id="A87487-001-a-0030">King</w>
     <pc xml:id="A87487-001-a-0040">,</pc>
     <w lemma="a" pos="d" xml:id="A87487-001-a-0050">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A87487-001-a-0060">PROCLAMATION</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-0070">OF</w>
     <w lemma="pardon" pos="n1" xml:id="A87487-001-a-0080">PARDON</w>
     <pc unit="sentence" xml:id="A87487-001-a-0090">.</pc>
    </head>
    <byline xml:id="A87487-e30">
     <w lemma="JAMES" pos="nn1" xml:id="A87487-001-a-0100">JAMES</w>
     <w lemma="r." pos="ab" xml:id="A87487-001-a-0110">R.</w>
     <pc unit="sentence" xml:id="A87487-001-a-0120"/>
    </byline>
    <p xml:id="A87487-e40">
     <w lemma="forasmuch" pos="av" rend="decorinit" xml:id="A87487-001-a-0130">FOrasmuch</w>
     <w lemma="as" pos="acp" xml:id="A87487-001-a-0140">as</w>
     <w lemma="several" pos="j" xml:id="A87487-001-a-0150">several</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-0160">of</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-0170">Our</w>
     <w lemma="subject" pos="n2" xml:id="A87487-001-a-0180">Subjects</w>
     <w lemma="have" pos="vvb" xml:id="A87487-001-a-0190">have</w>
     <w lemma="be" pos="vvn" xml:id="A87487-001-a-0200">been</w>
     <w lemma="seduce" pos="vvn" xml:id="A87487-001-a-0210">Seduced</w>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-0220">to</w>
     <w lemma="take" pos="vvi" xml:id="A87487-001-a-0230">take</w>
     <w lemma="up" pos="acp" xml:id="A87487-001-a-0240">up</w>
     <w lemma="arm" pos="n2" xml:id="A87487-001-a-0250">Arms</w>
     <pc xml:id="A87487-001-a-0260">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0270">and</w>
     <w lemma="contrary" pos="j" xml:id="A87487-001-a-0280">contrary</w>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-0290">to</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-0300">the</w>
     <w lemma="law" pos="n2" xml:id="A87487-001-a-0310">Laws</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-0320">of</w>
     <w lemma="God" pos="nn1" xml:id="A87487-001-a-0330">God</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0340">and</w>
     <w lemma="man" pos="n1" xml:id="A87487-001-a-0350">Man</w>
     <pc xml:id="A87487-001-a-0360">,</pc>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-0370">to</w>
     <w lemma="join" pos="vvi" reg="join" xml:id="A87487-001-a-0380">joyn</w>
     <w lemma="themselves" pos="pr" xml:id="A87487-001-a-0390">themselves</w>
     <w lemma="with" pos="acp" xml:id="A87487-001-a-0400">with</w>
     <w lemma="foreigner" pos="n2" xml:id="A87487-001-a-0410">Foreigners</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0420">and</w>
     <w lemma="stranger" pos="n2" xml:id="A87487-001-a-0430">Strangers</w>
     <pc xml:id="A87487-001-a-0440">,</pc>
     <w lemma="in" pos="acp" xml:id="A87487-001-a-0450">in</w>
     <w lemma="a" pos="d" xml:id="A87487-001-a-0460">a</w>
     <w lemma="most" pos="avs-d" xml:id="A87487-001-a-0470">most</w>
     <w lemma="unnatural" pos="j" reg="Unnatural" xml:id="A87487-001-a-0480">Vnnatural</w>
     <w lemma="invasion" pos="n1" xml:id="A87487-001-a-0490">Invasion</w>
     <w lemma="upon" pos="acp" xml:id="A87487-001-a-0500">upon</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A87487-001-a-0510">Vs</w>
     <pc xml:id="A87487-001-a-0520">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0530">and</w>
     <w lemma="this" pos="d" xml:id="A87487-001-a-0540">this</w>
     <w lemma="their" pos="po" xml:id="A87487-001-a-0550">their</w>
     <w lemma="native" pos="j" xml:id="A87487-001-a-0560">Native</w>
     <w lemma="country" pos="n1" xml:id="A87487-001-a-0570">Country</w>
     <pc xml:id="A87487-001-a-0580">,</pc>
     <w lemma="many" pos="d" xml:id="A87487-001-a-0590">many</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-0600">of</w>
     <w lemma="who" pos="crq" xml:id="A87487-001-a-0610">whom</w>
     <w lemma="we" pos="pns" xml:id="A87487-001-a-0620">We</w>
     <w lemma="be" pos="vvb" xml:id="A87487-001-a-0630">are</w>
     <w lemma="persuade" pos="vvn" reg="persuaded" xml:id="A87487-001-a-0640">perswaded</w>
     <w lemma="have" pos="vvb" xml:id="A87487-001-a-0650">have</w>
     <w lemma="be" pos="vvn" xml:id="A87487-001-a-0660">been</w>
     <w lemma="wrought" pos="vvn" xml:id="A87487-001-a-0670">wrought</w>
     <w lemma="upon" pos="acp" xml:id="A87487-001-a-0680">upon</w>
     <w lemma="by" pos="acp" xml:id="A87487-001-a-0690">by</w>
     <w lemma="false" pos="j" xml:id="A87487-001-a-0700">false</w>
     <w lemma="suggestion" pos="n2" xml:id="A87487-001-a-0710">Suggestions</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0720">and</w>
     <w lemma="misrepresentation" pos="n2" xml:id="A87487-001-a-0730">Misrepresentations</w>
     <w lemma="make" pos="vvn" xml:id="A87487-001-a-0740">made</w>
     <w lemma="by" pos="acp" xml:id="A87487-001-a-0750">by</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-0760">Our</w>
     <w lemma="enemy" pos="n2" xml:id="A87487-001-a-0770">Enemies</w>
     <pc xml:id="A87487-001-a-0780">:</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0790">And</w>
     <w lemma="we" pos="pns" xml:id="A87487-001-a-0800">We</w>
     <w lemma="desire" pos="vvg" xml:id="A87487-001-a-0810">desiring</w>
     <pc join="right" xml:id="A87487-001-a-0820">(</pc>
     <w lemma="as" pos="acp" xml:id="A87487-001-a-0830">as</w>
     <w lemma="far" pos="av-j" xml:id="A87487-001-a-0840">far</w>
     <w lemma="as" pos="acp" xml:id="A87487-001-a-0850">as</w>
     <w lemma="be" pos="vvz" xml:id="A87487-001-a-0860">is</w>
     <w lemma="possible" pos="j" xml:id="A87487-001-a-0870">possible</w>
     <pc xml:id="A87487-001-a-0880">)</pc>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-0890">to</w>
     <w lemma="reduce" pos="vvi" xml:id="A87487-001-a-0900">reduce</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-0910">Our</w>
     <w lemma="say" pos="j-vn" xml:id="A87487-001-a-0920">said</w>
     <w lemma="subject" pos="n2" xml:id="A87487-001-a-0930">Subjects</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-0940">to</w>
     <w lemma="duty" pos="n1" xml:id="A87487-001-a-0950">Duty</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-0960">and</w>
     <w lemma="obedience" pos="n1" xml:id="A87487-001-a-0970">Obedience</w>
     <w lemma="by" pos="acp" xml:id="A87487-001-a-0980">by</w>
     <w lemma="act" pos="n2" xml:id="A87487-001-a-0990">Acts</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-1000">of</w>
     <w lemma="clemency" pos="n1" xml:id="A87487-001-a-1010">Clemency</w>
     <pc xml:id="A87487-001-a-1020">,</pc>
     <w lemma="at" pos="acp" xml:id="A87487-001-a-1030">at</w>
     <w lemma="least" pos="ds" xml:id="A87487-001-a-1040">least</w>
     <w lemma="resolve" pos="vvg" xml:id="A87487-001-a-1050">resolving</w>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-1060">to</w>
     <w lemma="leave" pos="vvi" xml:id="A87487-001-a-1070">leave</w>
     <w lemma="all" pos="d" xml:id="A87487-001-a-1080">all</w>
     <w lemma="such" pos="d" xml:id="A87487-001-a-1090">such</w>
     <w lemma="as" pos="acp" xml:id="A87487-001-a-1100">as</w>
     <w lemma="shall" pos="vmb" xml:id="A87487-001-a-1110">shall</w>
     <w lemma="persist" pos="vvi" xml:id="A87487-001-a-1120">persist</w>
     <w lemma="in" pos="acp" xml:id="A87487-001-a-1130">in</w>
     <w lemma="so" pos="av" xml:id="A87487-001-a-1140">so</w>
     <w lemma="wicked" pos="j" xml:id="A87487-001-a-1150">wicked</w>
     <w lemma="a" pos="d" xml:id="A87487-001-a-1160">an</w>
     <w lemma="enterprise" pos="n1" reg="Enterprise" xml:id="A87487-001-a-1170">Enterprize</w>
     <pc xml:id="A87487-001-a-1180">,</pc>
     <w lemma="without" pos="acp" xml:id="A87487-001-a-1190">without</w>
     <w lemma="excuse" pos="n1" xml:id="A87487-001-a-1200">Excuse</w>
     <pc xml:id="A87487-001-a-1210">,</pc>
     <w lemma="do" pos="vvb" xml:id="A87487-001-a-1220">Do</w>
     <w lemma="therefore" pos="av" xml:id="A87487-001-a-1230">therefore</w>
     <w lemma="promise" pos="vvb" xml:id="A87487-001-a-1240">Promise</w>
     <pc xml:id="A87487-001-a-1250">,</pc>
     <w lemma="grant" pos="vvb" xml:id="A87487-001-a-1260">Grant</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1270">and</w>
     <w lemma="declare" pos="vvb" xml:id="A87487-001-a-1280">Declare</w>
     <pc xml:id="A87487-001-a-1290">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1300">and</w>
     <w lemma="by" pos="acp" xml:id="A87487-001-a-1310">by</w>
     <w lemma="this" pos="d" xml:id="A87487-001-a-1320">this</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-1330">Our</w>
     <w lemma="royal" pos="j" xml:id="A87487-001-a-1340">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A87487-001-a-1350">Proclamation</w>
     <w lemma="publish" pos="vvb" xml:id="A87487-001-a-1360">Publish</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-1370">Our</w>
     <w lemma="free" pos="j" xml:id="A87487-001-a-1380">Free</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1390">and</w>
     <w lemma="absolute" pos="j" xml:id="A87487-001-a-1400">Absolute</w>
     <w lemma="pardon" pos="n1" xml:id="A87487-001-a-1410">Pardon</w>
     <pc xml:id="A87487-001-a-1420">,</pc>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-1430">to</w>
     <w lemma="all" pos="d" xml:id="A87487-001-a-1440">all</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-1450">Our</w>
     <w lemma="subject" pos="n2" xml:id="A87487-001-a-1460">Subjects</w>
     <w lemma="who" pos="crq" xml:id="A87487-001-a-1470">who</w>
     <w lemma="have" pos="vvb" xml:id="A87487-001-a-1480">have</w>
     <w lemma="take" pos="vvn" xml:id="A87487-001-a-1490">taken</w>
     <w lemma="up" pos="acp" xml:id="A87487-001-a-1500">up</w>
     <w lemma="arm" pos="n2" xml:id="A87487-001-a-1510">Arms</w>
     <pc xml:id="A87487-001-a-1520">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1530">and</w>
     <w lemma="join" pos="vvn" reg="joined" xml:id="A87487-001-a-1540">joyned</w>
     <w lemma="with" pos="acp" xml:id="A87487-001-a-1550">with</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-1560">the</w>
     <w lemma="prince" pos="n1" xml:id="A87487-001-a-1570">Prince</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-1580">of</w>
     <w lemma="Orange" pos="nn1" rend="hi" xml:id="A87487-001-a-1590">Orange</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1600">and</w>
     <w lemma="his" pos="po" xml:id="A87487-001-a-1610">his</w>
     <w lemma="adherent" pos="n2" xml:id="A87487-001-a-1620">Adherents</w>
     <pc xml:id="A87487-001-a-1630">,</pc>
     <w lemma="in" pos="acp" xml:id="A87487-001-a-1640">in</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-1650">the</w>
     <w lemma="present" pos="j" xml:id="A87487-001-a-1660">present</w>
     <w lemma="invasion" pos="n1" xml:id="A87487-001-a-1670">Invasion</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-1680">of</w>
     <w lemma="this" pos="d" xml:id="A87487-001-a-1690">this</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-1700">Our</w>
     <w lemma="kingdom" pos="n1" xml:id="A87487-001-a-1710">Kingdom</w>
     <pc xml:id="A87487-001-a-1720">,</pc>
     <w lemma="provide" pos="vvn" xml:id="A87487-001-a-1730">Provided</w>
     <w lemma="they" pos="pns" xml:id="A87487-001-a-1740">they</w>
     <w lemma="quit" pos="vvb" xml:id="A87487-001-a-1750">quit</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1760">and</w>
     <w lemma="desert" pos="vvi" xml:id="A87487-001-a-1770">desert</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-1780">Our</w>
     <w lemma="say" pos="j-vn" xml:id="A87487-001-a-1790">said</w>
     <w lemma="enemy" pos="n2" xml:id="A87487-001-a-1800">Enemies</w>
     <pc xml:id="A87487-001-a-1810">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-1820">and</w>
     <w lemma="within" pos="acp" xml:id="A87487-001-a-1830">within</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-1840">the</w>
     <w lemma="space" pos="n1" xml:id="A87487-001-a-1850">space</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-1860">of</w>
     <w lemma="twenty" pos="crd" xml:id="A87487-001-a-1870">Twenty</w>
     <w lemma="day" pos="n2" xml:id="A87487-001-a-1880">Days</w>
     <w lemma="from" pos="acp" xml:id="A87487-001-a-1890">from</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-1900">the</w>
     <w lemma="date" pos="n1" xml:id="A87487-001-a-1910">Date</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-1920">of</w>
     <w lemma="this" pos="d" xml:id="A87487-001-a-1930">this</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-1940">Our</w>
     <w lemma="royal" pos="j" xml:id="A87487-001-a-1950">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A87487-001-a-1960">Proclamation</w>
     <pc xml:id="A87487-001-a-1970">,</pc>
     <w lemma="render" pos="vvb" xml:id="A87487-001-a-1980">render</w>
     <w lemma="themselves" pos="pr" xml:id="A87487-001-a-1990">themselves</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-2000">to</w>
     <w lemma="some" pos="d" xml:id="A87487-001-a-2010">some</w>
     <w lemma="one" pos="pi" xml:id="A87487-001-a-2020">one</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-2030">of</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-2040">Our</w>
     <w lemma="officer" pos="n2" xml:id="A87487-001-a-2050">Officers</w>
     <w lemma="civil" pos="j" xml:id="A87487-001-a-2060">Civil</w>
     <w lemma="or" pos="cc" xml:id="A87487-001-a-2070">or</w>
     <w lemma="military" pos="j" xml:id="A87487-001-a-2080">Military</w>
     <pc xml:id="A87487-001-a-2090">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2100">and</w>
     <w lemma="do" pos="vvb" xml:id="A87487-001-a-2110">do</w>
     <w lemma="not" pos="xx" xml:id="A87487-001-a-2120">not</w>
     <w lemma="again" pos="av" xml:id="A87487-001-a-2130">again</w>
     <pc xml:id="A87487-001-a-2140">,</pc>
     <w lemma="after" pos="acp" xml:id="A87487-001-a-2150">after</w>
     <w lemma="they" pos="pns" xml:id="A87487-001-a-2160">they</w>
     <w lemma="have" pos="vvb" xml:id="A87487-001-a-2170">have</w>
     <w lemma="render" pos="vvn" xml:id="A87487-001-a-2180">rendered</w>
     <w lemma="themselves" pos="pr" xml:id="A87487-001-a-2190">themselves</w>
     <w lemma="as" pos="acp" xml:id="A87487-001-a-2200">as</w>
     <w lemma="aforesaid" pos="j" xml:id="A87487-001-a-2210">aforesaid</w>
     <pc xml:id="A87487-001-a-2220">,</pc>
     <w lemma="return" pos="vvb" xml:id="A87487-001-a-2230">return</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-2240">to</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-2250">Our</w>
     <w lemma="enemy" pos="n2" xml:id="A87487-001-a-2260">Enemies</w>
     <pc xml:id="A87487-001-a-2270">,</pc>
     <w lemma="or" pos="cc" xml:id="A87487-001-a-2280">or</w>
     <w lemma="be" pos="vvi" xml:id="A87487-001-a-2290">be</w>
     <w lemma="any" pos="d" xml:id="A87487-001-a-2300">any</w>
     <w lemma="way" pos="n1" xml:id="A87487-001-a-2310">way</w>
     <w lemma="aid" pos="vvg" xml:id="A87487-001-a-2320">Aiding</w>
     <w lemma="or" pos="cc" xml:id="A87487-001-a-2330">or</w>
     <w lemma="assist" pos="vvg" xml:id="A87487-001-a-2340">Assisting</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-2350">to</w>
     <w lemma="they" pos="pno" xml:id="A87487-001-a-2360">them</w>
     <pc xml:id="A87487-001-a-2370">:</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2380">And</w>
     <w lemma="they" pos="pns" xml:id="A87487-001-a-2390">they</w>
     <w lemma="who" pos="crq" xml:id="A87487-001-a-2400">who</w>
     <w lemma="refuse" pos="vvb" xml:id="A87487-001-a-2410">refuse</w>
     <w lemma="or" pos="cc" xml:id="A87487-001-a-2420">or</w>
     <w lemma="neglect" pos="vvi" xml:id="A87487-001-a-2430">neglect</w>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-2440">to</w>
     <w lemma="lay" pos="vvi" xml:id="A87487-001-a-2450">lay</w>
     <w lemma="hold" pos="n1" xml:id="A87487-001-a-2460">hold</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-2470">of</w>
     <w lemma="this" pos="d" xml:id="A87487-001-a-2480">this</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-2490">Our</w>
     <w lemma="free" pos="j" xml:id="A87487-001-a-2500">Free</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2510">and</w>
     <w lemma="gracious" pos="j" xml:id="A87487-001-a-2520">Gracious</w>
     <w lemma="offer" pos="n1" xml:id="A87487-001-a-2530">Offer</w>
     <pc xml:id="A87487-001-a-2540">,</pc>
     <w lemma="must" pos="vmb" xml:id="A87487-001-a-2550">must</w>
     <w lemma="never" pos="avx" xml:id="A87487-001-a-2560">never</w>
     <w lemma="expect" pos="vvi" xml:id="A87487-001-a-2570">expect</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-2580">Our</w>
     <w lemma="pardon" pos="n1" xml:id="A87487-001-a-2590">Pardon</w>
     <w lemma="hereafter" pos="av" xml:id="A87487-001-a-2600">hereafter</w>
     <pc xml:id="A87487-001-a-2610">,</pc>
     <w lemma="but" pos="acp" xml:id="A87487-001-a-2620">but</w>
     <w lemma="will" pos="vmb" xml:id="A87487-001-a-2630">will</w>
     <w lemma="be" pos="vvi" xml:id="A87487-001-a-2640">be</w>
     <w lemma="whole" pos="av-j" xml:id="A87487-001-a-2650">wholly</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2660">and</w>
     <w lemma="just" pos="av-j" xml:id="A87487-001-a-2670">justly</w>
     <w lemma="exclude" pos="vvn" xml:id="A87487-001-a-2680">Excluded</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-2690">of</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2700">and</w>
     <w lemma="from" pos="acp" xml:id="A87487-001-a-2710">from</w>
     <w lemma="all" pos="d" xml:id="A87487-001-a-2720">all</w>
     <w lemma="hope" pos="n2" xml:id="A87487-001-a-2730">hopes</w>
     <w lemma="thereof" pos="av" xml:id="A87487-001-a-2740">thereof</w>
     <pc unit="sentence" xml:id="A87487-001-a-2750">.</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2760">And</w>
     <w lemma="last" pos="ord" xml:id="A87487-001-a-2770">lastly</w>
     <pc xml:id="A87487-001-a-2780">,</pc>
     <w lemma="we" pos="pns" xml:id="A87487-001-a-2790">We</w>
     <w lemma="also" pos="av" xml:id="A87487-001-a-2800">also</w>
     <w lemma="promise" pos="vvb" xml:id="A87487-001-a-2810">Promise</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2820">and</w>
     <w lemma="grant" pos="vvb" xml:id="A87487-001-a-2830">Grant</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-2840">Our</w>
     <w lemma="pardon" pos="n1" xml:id="A87487-001-a-2850">Pardon</w>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-2860">and</w>
     <w lemma="protection" pos="n1" xml:id="A87487-001-a-2870">Protection</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-2880">to</w>
     <w lemma="all" pos="d" xml:id="A87487-001-a-2890">all</w>
     <w lemma="such" pos="d" xml:id="A87487-001-a-2900">such</w>
     <w lemma="foreigner" pos="n2" xml:id="A87487-001-a-2910">Foreigners</w>
     <w lemma="as" pos="acp" xml:id="A87487-001-a-2920">as</w>
     <w lemma="do" pos="vvi" xml:id="A87487-001-a-2930">do</w>
     <w lemma="or" pos="cc" xml:id="A87487-001-a-2940">or</w>
     <w lemma="shall" pos="vmb" xml:id="A87487-001-a-2950">shall</w>
     <w lemma="come" pos="vvi" xml:id="A87487-001-a-2960">come</w>
     <w lemma="over" pos="acp" xml:id="A87487-001-a-2970">over</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-2980">to</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A87487-001-a-2990">Vs</w>
     <pc xml:id="A87487-001-a-3000">,</pc>
     <w lemma="who" pos="crq" xml:id="A87487-001-a-3010">whom</w>
     <w lemma="we" pos="pns" xml:id="A87487-001-a-3020">We</w>
     <w lemma="will" pos="vmb" xml:id="A87487-001-a-3030">will</w>
     <w lemma="either" pos="av-d" xml:id="A87487-001-a-3040">either</w>
     <w lemma="entertain" pos="vvi" xml:id="A87487-001-a-3050">Entertain</w>
     <w lemma="in" pos="acp" xml:id="A87487-001-a-3060">in</w>
     <w lemma="our" pos="po" xml:id="A87487-001-a-3070">Our</w>
     <w lemma="service" pos="n1" xml:id="A87487-001-a-3080">Service</w>
     <pc xml:id="A87487-001-a-3090">,</pc>
     <w lemma="or" pos="cc" xml:id="A87487-001-a-3100">or</w>
     <w lemma="otherwise" pos="av" xml:id="A87487-001-a-3110">otherwise</w>
     <w lemma="grant" pos="vvb" xml:id="A87487-001-a-3120">Grant</w>
     <w lemma="they" pos="pno" xml:id="A87487-001-a-3130">them</w>
     <pc join="right" xml:id="A87487-001-a-3140">(</pc>
     <w lemma="if" pos="cs" xml:id="A87487-001-a-3150">if</w>
     <w lemma="they" pos="pns" xml:id="A87487-001-a-3160">they</w>
     <w lemma="shall" pos="vmb" xml:id="A87487-001-a-3170">shall</w>
     <w lemma="desire" pos="vvi" xml:id="A87487-001-a-3180">desire</w>
     <w lemma="it" pos="pn" xml:id="A87487-001-a-3190">it</w>
     <pc xml:id="A87487-001-a-3200">)</pc>
     <w lemma="freedom" pos="n1" xml:id="A87487-001-a-3210">freedom</w>
     <w lemma="of" pos="acp" xml:id="A87487-001-a-3220">of</w>
     <w lemma="passage" pos="n1" xml:id="A87487-001-a-3230">Passage</w>
     <pc xml:id="A87487-001-a-3240">,</pc>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-3250">and</w>
     <w lemma="liberty" pos="n1" xml:id="A87487-001-a-3260">liberty</w>
     <w lemma="to" pos="prt" xml:id="A87487-001-a-3270">to</w>
     <w lemma="return" pos="vvi" xml:id="A87487-001-a-3280">return</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-3290">to</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-3300">the</w>
     <w lemma="respective" pos="j" xml:id="A87487-001-a-3310">respective</w>
     <w lemma="country" pos="n2" xml:id="A87487-001-a-3320">Countries</w>
     <w lemma="from" pos="acp" xml:id="A87487-001-a-3330">from</w>
     <w lemma="whence" pos="crq" xml:id="A87487-001-a-3340">whence</w>
     <w lemma="they" pos="pns" xml:id="A87487-001-a-3350">they</w>
     <w lemma="come" pos="vvd" xml:id="A87487-001-a-3360">came</w>
     <pc unit="sentence" xml:id="A87487-001-a-3370">.</pc>
    </p>
    <closer xml:id="A87487-e60">
     <dateline xml:id="A87487-e70">
      <w lemma="give" pos="vvn" xml:id="A87487-001-a-3380">Given</w>
      <w lemma="at" pos="acp" xml:id="A87487-001-a-3390">at</w>
      <w lemma="our" pos="po" xml:id="A87487-001-a-3400">Our</w>
      <w lemma="court" pos="n1" xml:id="A87487-001-a-3410">Court</w>
      <w lemma="at" pos="acp" xml:id="A87487-001-a-3420">at</w>
      <w lemma="Salisbury" pos="nn1" rend="hi" xml:id="A87487-001-a-3430">Salisbury</w>
      <date xml:id="A87487-e90">
       <w lemma="the" pos="d" xml:id="A87487-001-a-3440">the</w>
       <w lemma="22th" pos="ord" rend="hi" xml:id="A87487-001-a-3450">22th</w>
       <w lemma="day" pos="n1" xml:id="A87487-001-a-3460">Day</w>
       <w lemma="of" pos="acp" xml:id="A87487-001-a-3470">of</w>
       <w lemma="November" pos="nn1" rend="hi" xml:id="A87487-001-a-3480">November</w>
       <w lemma="1688." pos="crd" xml:id="A87487-001-a-3490">1688.</w>
       <pc unit="sentence" xml:id="A87487-001-a-3500"/>
      </date>
      <w lemma="in" pos="acp" xml:id="A87487-001-a-3510">In</w>
      <w lemma="the" pos="d" xml:id="A87487-001-a-3520">the</w>
      <w lemma="four" pos="ord" xml:id="A87487-001-a-3530">Fourth</w>
      <w lemma="year" pos="n1" xml:id="A87487-001-a-3540">Year</w>
      <w lemma="of" pos="acp" xml:id="A87487-001-a-3550">of</w>
      <w lemma="our" pos="po" xml:id="A87487-001-a-3560">Our</w>
      <w lemma="reign" pos="n1" xml:id="A87487-001-a-3570">Reign</w>
      <pc unit="sentence" xml:id="A87487-001-a-3580">.</pc>
     </dateline>
     <w lemma="God" pos="nn1" xml:id="A87487-001-a-3590">God</w>
     <w lemma="save" pos="vvb" xml:id="A87487-001-a-3600">save</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-3610">the</w>
     <w lemma="king" pos="n1" xml:id="A87487-001-a-3620">King</w>
     <pc unit="sentence" xml:id="A87487-001-a-3621">.</pc>
     <pc unit="sentence" xml:id="A87487-001-a-3630"/>
    </closer>
   </div>
  </body>
  <back xml:id="A87487-e120">
   <div type="colophon" xml:id="A87487-e130">
    <p xml:id="A87487-e140">
     <hi xml:id="A87487-e150">
      <w lemma="LONDON" pos="nn1" xml:id="A87487-001-a-3640">LONDON</w>
      <pc xml:id="A87487-001-a-3650">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A87487-001-a-3660">Printed</w>
     <w lemma="by" pos="acp" xml:id="A87487-001-a-3670">by</w>
     <hi xml:id="A87487-e160">
      <w lemma="Charles" pos="nn1" xml:id="A87487-001-a-3680">Charles</w>
      <w lemma="bill" pos="n1" xml:id="A87487-001-a-3690">Bill</w>
      <pc xml:id="A87487-001-a-3700">,</pc>
      <w lemma="Henry" pos="nn1" xml:id="A87487-001-a-3710">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A87487-001-a-3720">Hills</w>
      <pc xml:id="A87487-001-a-3730">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A87487-001-a-3740">and</w>
     <hi xml:id="A87487-e170">
      <w lemma="Thomas" pos="nn1" xml:id="A87487-001-a-3750">Thomas</w>
      <w lemma="newcomb" pos="nn1" xml:id="A87487-001-a-3760">Newcomb</w>
      <pc xml:id="A87487-001-a-3770">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A87487-001-a-3780">Printers</w>
     <w lemma="to" pos="acp" xml:id="A87487-001-a-3790">to</w>
     <w lemma="the" pos="d" xml:id="A87487-001-a-3800">the</w>
     <w lemma="king" pos="n2" xml:id="A87487-001-a-3810">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A87487-001-a-3820">most</w>
     <w lemma="excellent" pos="j" xml:id="A87487-001-a-3830">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A87487-001-a-3840">Majesty</w>
     <pc unit="sentence" xml:id="A87487-001-a-3850">.</pc>
     <w lemma="1688." pos="crd" xml:id="A87487-001-a-3860">1688.</w>
     <pc unit="sentence" xml:id="A87487-001-a-3870"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
